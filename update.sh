#!/bin/bash
yum -y update
# update script
cd

rm /usr/bin/user-add
rm /usr/bin/user-list
rm /usr/bin/user-login
rm /usr/bin/user-expire
rm /usr/bin/user-limit
rm /usr/bin/trial
rm /usr/bin/scan-virus
rm /usr/bin/user-del
rm /usr/bin/user-lock

curl https://gitlab.com/presult77/autoscript/raw/master/useradd.html >> /usr/bin/user-add
curl https://gitlab.com/presult77/autoscript/raw/master/userlist.html >> /usr/bin/user-list
curl https://gitlab.com/presult77/autoscript/raw/master/userlogin.html >> /usr/bin/user-login
curl https://gitlab.com/presult77/autoscript/raw/master/userexpire.html >> /usr/bin/user-expire
curl https://gitlab.com/presult77/autoscript/raw/master/userlimit.html >> /usr/bin/user-limit
curl https://gitlab.com/presult77/autoscript/raw/master/trial.html >> /usr/bin/trial
curl https://gitlab.com/presult77/autoscript/raw/master/scan-virus.html >> /usr/bin/scan-virus
curl https://gitlab.com/presult77/autoscript/raw/master/userdel.html >> /usr/bin/user-del
curl https://gitlab.com/presult77/autoscript/raw/master/user-lock.html >> /usr/bin/user-lock

chmod +x /usr/bin/user-add
chmod +x /usr/bin/user-list
chmod +x /usr/bin/user-login
chmod +x /usr/bin/user-expire
chmod +x /usr/bin/user-limit
chmod +x /usr/bin/trial
chmod +x /usr/bin/scan-virus
chmod +x /usr/bin/user-del
chmod +x /usr/bin/user-lock

sed -i '$ i\0 */12 * * * root /usr/bin/user-lock' /etc/crontab

rm update.sh